# The travelling salesman problem #
## Algorithm
A parallel branch and bound algorithm [1] that solves the asymmetric traveling salesman problem to optimality is described. The algorithm uses an assignment problem based lower bounding technique, subtour elimination branching rules, and a subtour patching algorithm as an upper bounding procedure. The algorithm is organized around a data flow framework for parallel branch and bound. The algorithm begins by converting the cost matrix to a sparser version in such a fashion as to retain the optimality of the final solution. 

## Start
Algorithm is written in Julia language. 
To start type command below:
```
include("The_travelling_salesman_problem.jl")
```


## References
[1] Pekny, J. F., & Miller, D. L. (1992). A parallel branch and bound algorithm for solving large asymmetric traveling salesman problems. Mathematical Programming, 55(1), 17–33. https://doi.org/10.1007/BF01581188
