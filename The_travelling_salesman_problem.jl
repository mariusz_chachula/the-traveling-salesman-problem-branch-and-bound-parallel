@everywhere module TreeModule
    # TREE 

    export TreeNode, LenghtKey

    type TreeNode
        value::Int
        length::Float32
        parent::Nullable{TreeNode} 
    end

    type Best
        best::Nullable{TreeNode} 
    end
    type LenghtKey
        length::Float32
        key::Int
    end


    function addChild(treeNode::TreeNode, val::Int, length)
        return TreeNode(val, length, treeNode)
    end

    function getParents(treeNode::TreeNode)
        parents=Vector{Int}()
    
        push!(parents, treeNode.value)
        while !isnull(treeNode.parent)
            treeNode=get(treeNode.parent)
            push!(parents, treeNode.value)
        end
        
        return parents
    end

 
    # TREE 

function getLength(city1::Int, city2::Int, cities)
    return cities[city1, city2]
end

function contains(list::Vector{Int}, value)
    if findfirst(list, value)!=0
        return true
    else
        return false
    end
end




function getTraceLength(treeNode::TreeNode)
    taceLength=0.0
    
    taceLength+=treeNode.length
    while !isnull(treeNode.parent)
        treeNode=get(treeNode.parent)
        taceLength+=treeNode.length
    end

    return taceLength
end


function getTraceLengthAndKey(treeNode::TreeNode)
    lenghtKey=LenghtKey(0.0, 0)
    
    lenghtKey.length+=treeNode.length
    lenghtKey.key+=2^treeNode.value
    while !isnull(treeNode.parent)
        treeNode=get(treeNode.parent)

        lenghtKey.length+=treeNode.length
        lenghtKey.key+=2^treeNode.value
    end

    return lenghtKey
end


function geterateRouteTreeLevels(maxLvl::Int, currentLvl::Int, treeNode::TreeNode, cities, leafs)
        parents=getParents(treeNode)

        if currentLvl<maxLvl
            for i=1:citiesCount
                if !contains(parents, i)
                    child=addChild(treeNode, i, getLength(treeNode.value , i, cities))
                    geterateRouteTreeLevels(maxLvl, currentLvl+1, child, cities, leafs)
                end
            end
        else
            push!(leafs, treeNode)
        end

end

function geterateRouteTree(cities)
    rootNode=TreeNode(1, 0, Nullable{}())
    leafs=Vector{TreeNode}()

    for i=1:citiesCount
        if !contains(getParents(rootNode), i)
            child=addChild(rootNode, i, getLength(1, i, cities))
            geterateRouteTreeLevels(startNodeLevel-1, 1, child, cities, leafs)
        end
    end

    return leafs
end


function prune(node::TreeNode, minimumDistance, keyValue)
    if !haskey(minimumDistance, [keyValue[3], keyValue[1]])
       #println("NEW ", keyValue[1], " -- ", keyValue[2]," -> ", " -> ", getParents(node))
        minimumDistance[[keyValue[3], keyValue[1]]]=keyValue[2]
        return false 
    elseif minimumDistance[[keyValue[3], keyValue[1]]]>=keyValue[2]
        #println("MIN ", keyValue[1], " -- ", keyValue[2]," -> ", minimumDistance[[keyValue[3], keyValue[1]]], " -> ", getParents(node))
        minimumDistance[[keyValue[3], keyValue[1]]]=keyValue[2]
        return false
    else
        #println("PPRUNE ", keyValue[1], " -- ", keyValue[2]," -> ", minimumDistance[[keyValue[3], keyValue[1]]], " -> ", getParents(node))
        return true
    end
end



function branchAndBoundEvaluate(node::TreeNode, parents::Vector{Int}, minimumDistance, keyValue, bestResult::Best, citiesCount::Int, cities)
       
        endNode=true
        # tworzenie dzieci
        for i=1:citiesCount
            if !contains(parents, i)
                endNode=false
                childNode=addChild(node, i, getLength(node.value , i, cities))
                newKeyLength=[keyValue[1]+2^childNode.value, keyValue[2]+childNode.length, childNode.value]
            
                if !prune(childNode, minimumDistance, newKeyLength)
                    branchAndBoundEvaluate(childNode, cat(1, parents, [childNode.value]), minimumDistance, newKeyLength, bestResult, citiesCount, cities)
                end
            end
        end

        if endNode && (isnull(bestResult.best) || keyValue[2]+getLength(node.value , 1, cities)<getTraceLength(get(bestResult.best)))
            child=addChild(node, 1, getLength(node.value , 1, cities))
            bestResult.best=Nullable{TreeNode}(child)
            println(">>>>>>>>>>",getParents(get(bestResult.best)), ", ",getTraceLength(get(bestResult.best)), "<<<<<<<<<<<<<<<")
        end
end


function branchAndBoundStartEvaluate(node::TreeNode, parents::Vector{Int}, minimumDistance, keyValue, bestResult::Best, citiesCount::Int, cities)
       
        endNode=true
        # tworzenie dzieci
        for i=1:citiesCount
            if !contains(parents, i)
                endNode=false
                childNode=addChild(node, i, getLength(node.value , i, cities))
                newKeyLength=[keyValue[1]+2^childNode.value, keyValue[2]+childNode.length, childNode.value]
            
                if !prune(childNode, minimumDistance, newKeyLength)
                    branchAndBoundEvaluate(childNode, cat(1, parents, [childNode.value]), minimumDistance, newKeyLength, bestResult, citiesCount, cities)
                end
            end
        end
        return bestResult
end



function branchAndBound(nodes::Vector{TreeNode}, cities, citiesCount)
    
    np = nprocs()
    i=1
    nextChild() = (idx=i; i+=1; idx)


     @sync begin
        
            for p=1:np
                if p != myid() || np == 1
                    @async begin
                        #using TreeModule
                        minimumDistance=Dict()
                        bestResult=Best(Nullable{TreeNode}())
                        while true
                            idx = nextChild()
                            if (idx >=size(nodes, 1))
                                break;
                            end
                            
                             println("Leaf: ",getParents(nodes[idx]))
                            traceAndLength= getTraceLengthAndKey(nodes[idx])                                  
                            bestResult=remotecall_fetch(TreeModule.branchAndBoundStartEvaluate, p, nodes[idx], getParents(nodes[idx]), minimumDistance, [traceAndLength.key, traceAndLength.length, nodes[idx].value], bestResult, citiesCount, cities)
                            
                            #println(traceAndLength.key, " - > ", traceAndLength.length)
                            #remotecall_fetch(test, p)
                            #println(">>>>>>>>>>",getParents(get(bestResult.best)), ", ",getTraceLength(get(bestResult.best)), "<<<<<<<<<<<<<<< Start: ", getParents(nodes[idx]), " -> ", nodes[idx].value)
                        end
                        #println("MIN >>>>>>>>>>",getParents(get(bestResult.best)), ", ",getTraceLength(get(bestResult.best)), "<<<<<<<<<<<<<<<")
                        
                    end
                end
            end
      end
end

function generatCities()
  srand(1452)
  A = rand(Float16, citiesCount,citiesCount)                                                                                                                                                                         
  Cities= (A + A')/sqrt(2)*1000

  for i in 1:citiesCount 
    Cities[i, i]=0.0
  end
  # println(Cities)
  # println(Symmetric(Cities))

  return Symmetric(Cities)
end

function start(_citiesCount, _startNodeLevel)
    global bestResult=Nullable{TreeNode}()
    global citiesCount=_citiesCount
    global startNodeLevel=_startNodeLevel
    global cities = generatCities()
    nodes = geterateRouteTree(cities)
    startTime=now()
    branchAndBound(nodes, cities, citiesCount)
    endTime=now()-startTime
    println(startNodeLevel, "\t",  nprocs(), "\t", endTime, "\t", citiesCount)
end

end

    for rootLevel in 2:4
        for problem in 6:6
            TreeModule.start(problem, rootLevel)
        end
    end
